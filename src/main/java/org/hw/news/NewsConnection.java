package org.hw.news;

import java.net.http.HttpRequest;

public interface NewsConnection {
    HttpRequest getRequest();
}
