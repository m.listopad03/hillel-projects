package org.hw.news;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class GoogleNewsConnection implements NewsConnection {

    @Override
    public HttpRequest getRequest() {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://google-news.p.rapidapi.com/v1/source_search?source=nytimes.com&lang=en&country=US"))
                .header("x-rapidapi-key", "f02452bcc1msh6b785737344d158p18831bjsnede788b69207")
                .header("x-rapidapi-host", "google-news.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();

        return request;
    }
}




