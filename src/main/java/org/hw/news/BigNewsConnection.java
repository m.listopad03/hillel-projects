package org.hw.news;

import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpRequest;
@Component
public class BigNewsConnection implements NewsConnection{


        @Override
        public HttpRequest getRequest() {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("https://bing-news-search1.p.rapidapi.com/news/trendingtopics?safeSearch=Off&textFormat=Raw"))
                        .header("x-bingapis-sdk", "true")
                        .header("x-rapidapi-key", "f02452bcc1msh6b785737344d158p18831bjsnede788b69207")
                        .header("x-rapidapi-host", "bing-news-search1.p.rapidapi.com")
                        .method("GET", HttpRequest.BodyPublishers.noBody())
                        .build();

                return request;
        }
}
