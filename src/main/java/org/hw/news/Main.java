package org.hw.news;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("context.xml");

        NewsConnection googleNews= applicationContext.getBean(GoogleNewsConnection.class);
        NewsConnection bigNews= applicationContext.getBean(BigNewsConnection.class);

        NewsService newsService= applicationContext.getBean(NewsService.class);
       newsService.showNews(bigNews);
    }
}
