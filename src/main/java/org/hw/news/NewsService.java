package org.hw.news;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
@RequiredArgsConstructor
public class NewsService {
    private final BigNewsConnection bigNewsConnection;
    private final GoogleNewsConnection googleNewsConnection;
    public void showNews(NewsConnection newsConnection){

        HttpRequest request = newsConnection.getRequest();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            System.out.println("disconnection with server");
        }
        System.out.println(response.body());
    }

}
